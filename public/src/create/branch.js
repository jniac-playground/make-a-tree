import { createBox } from './basic/box.js'
import { createGroup } from './basic/group.js'

export const createBranch = ({

	recursivity = 4,
	recursiveStep = 0,
	length = 1.5,
	diameter = .2,
	onEnding = null,
	...otherProps

} = {}) => {

	const branch = createGroup(otherProps)
	branch.updateWorldMatrix()

	createBox({
		width: diameter,
		depth: diameter,
		height: length,
		y: length / 2,
		castShadow: true,
		parent: branch,
	})

	if (recursiveStep < recursivity) {

		createBranch({
			y: length,
			rotationZ: -20,
			rotationY: -170,
			scale: .8,
			recursiveStep: recursiveStep + 1,
			recursivity,
			length,
			diameter,
			onEnding,
			parent: branch,
		})

		createBranch({
			y: length,
			rotationZ: -30,
			rotationY: -30,
			scale: .75,
			recursiveStep: recursiveStep + 1,
			recursivity,
			length,
			diameter,
			onEnding,
			parent: branch,
		})

	} else {

		onEnding?.(branch)
	}

	return branch
}
