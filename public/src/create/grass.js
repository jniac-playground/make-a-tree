import * as THREE from '../three/three.module.js'
import { arrange } from './basic/arrange.js'
import { createGroup } from './basic/group.js'
import { createMesh } from './basic/mesh.js'
import { lerp } from '../utils.js'

const flowerGeometry = new THREE.PlaneGeometry(.1, .1, 1, 1)
const createFlower = (props) => {
	const color = Math.random() < .8 ? 'white' : '#fc0'
	const plane = createMesh({ 
		...props,
		rotationX: -90,
		rotationY: Math.random() * 360,
		rotationZ: (Math.random() - 0.5) * 30,
		rotationOrder: 'ZYX',
		color,
		geometry: flowerGeometry,
		side: THREE.DoubleSide,
		receiveShadow: true,
	})
	createMesh({ 
		parent: plane,
		z: 0.02,
		rotationZ: 45,
		color,
		geometry: flowerGeometry,
		side: THREE.DoubleSide,
		receiveShadow: true,
	})
	return plane
}

export const createGrass = ({
	
	count = 400,
	size = 1,
	heightMin = 0.1,
	heightMax = 0.2,
	color = '#fff',
	flowerProbability = 0.05,
	...layoutProps

} = {}) => {

	const flowers = createGroup()

	const points = []
	for (let i = 0; i < count; i++) {

		let x = size * (Math.random() - 0.5)
		let y = 0
		let z = size * (Math.random() - 0.5)
		const start = new THREE.Vector3(x, y, z)

		x += .1 * (Math.random() - 0.5)
		z += .1 * (Math.random() - 0.5)
		y += lerp(heightMin, heightMax, Math.random())
		const end = new THREE.Vector3(x, y, z)
		
		points.push(start, end)

		if (Math.random() < flowerProbability) {
			createFlower({ x, y, z, parent:flowers })
		}
	}

	const geometry = new THREE.BufferGeometry().setFromPoints(points)
	const material = new THREE.LineBasicMaterial({ color })
	const grass = new THREE.LineSegments(geometry, material)

	arrange(grass, layoutProps)

	grass.add(flowers)

	return grass
}