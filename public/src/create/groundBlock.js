import * as THREE from '../three/three.module.js'
import { createGroup } from './basic/group.js'
import { createMesh } from './basic/mesh.js'
import { createGrass } from './grass.js'
import { fbxLoader } from '../stage/loaders.js'
import { randomItem } from '../utils.js'

const groundGeometry = new THREE.BoxGeometry()

fbxLoader.load('./assets/rounded-box.fbx', (group) => {
  const geometry = group.children[0].geometry
  const scalar = 0.01
  geometry.scale(scalar, scalar, scalar)
  groundGeometry.copy(group.children[0].geometry)
})

export const createGroundBlock = ({

  size = 1,
  ...props

} = {}) => {

  const group = createGroup(props)
  const groundMaterial = new THREE.MeshPhysicalMaterial({
    color: randomItem(['#f06e2d', '#ddd', '#fff']),
  })

  createMesh({
    scale: size,
    y: -size / 2,
    geometry: groundGeometry,
    material: groundMaterial,
    parent: group,
    receiveShadow: true,
  })

  createGrass({
    count: 50,
    size,
    parent: group,
  })

  createGrass({
    count: 3,
    heightMin: .25,
    heightMax: .75,
    size,
    flowerProbability: 1,
    parent: group,
  })

  return group
}
