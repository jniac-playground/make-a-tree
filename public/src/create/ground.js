import { randomItem } from '../utils.js'
import { createGroundBlock } from './groundBlock.js'
import { createGroup } from './basic/group.js'

export const createGround = ({

	gridWidth = 5,
	gridHeight = 5,
	skip = () => false,
	blockProps = null,
	...props

} = {}) => {

	const group = createGroup(props)

	for (let j = 0; j < gridHeight; j++) {
		for (let i = 0; i < gridWidth; i++) {

			if (skip(i, j)) {
				continue
			}

			const x = i - (gridWidth - 1) / 2
			const z = j - (gridHeight - 1) / 2
			const y = randomItem([0, .25, .5])

			createGroundBlock({
				...blockProps,
				x, z, y,
				parent: group,
			})
		}
	}

	return group
}
