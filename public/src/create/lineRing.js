import * as THREE from '../three/three.module.js'
import { arrange } from './basic/arrange.js'

export const createLineRing = ({
	
	color = '#fc0',
	radius = 1,
	...otherProps

} = {}) => {

	// https://threejs.org/docs/?q=ell#api/en/extras/curves/EllipseCurve
	const curve = new THREE.EllipseCurve(
		0,  0,            // ax, aY
		radius, radius,   // xRadius, yRadius
		0,  2 * Math.PI,  // aStartAngle, aEndAngle
		false,            // aClockwise
		0                 // aRotation
	)

	const points = curve.getPoints(250)
	const geometry = new THREE.BufferGeometry().setFromPoints(points)
	const material = new THREE.LineBasicMaterial({ color })
	const ellipse = new THREE.Line(geometry, material)
	
	arrange(ellipse, otherProps)

	return ellipse
} 