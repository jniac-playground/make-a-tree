import * as THREE from '../three/three.module.js'
import { arrange } from './basic/arrange.js'
import { textureLoader } from '../stage/loaders.js'
import { randomItem } from '../utils.js'
import { orange } from '../config.js'
import { createMesh } from './basic/mesh.js'

const geometry = new THREE.PlaneGeometry()
const alphaMap = textureLoader.load('./assets/leaf-1.png')
const material = new THREE.MeshPhysicalMaterial({
	alphaMap,
	alphaTest: 0.5,
	transparent: true,
	side: THREE.DoubleSide,
})

export const createInstancedLeaves = ({

	maxCount = 2000,
	...otherProps

} = {}) => {

	// https://threejs.org/docs/#api/en/objects/InstancedMesh
	const instancedMesh = createMesh({
		...otherProps,
		geometry,
		material,
		instance: true,
		instanceCount: maxCount,
		castShadow: true,
	})
	instancedMesh.name = 'leaves'
	instancedMesh.customDepthMaterial = new THREE.MeshDepthMaterial({
		depthPacking: THREE.RGBADepthPacking,
		alphaMap: alphaMap,
		alphaTest: 0.5,
	})

	const dummy = new THREE.Object3D()
	const dummyColor = new THREE.Color()
	let index = 0
	const addLeaf = ({ x = 0, y = 0, z = 0, scale = 1, quaternion = null }) => {

		if (index === maxCount) {
			console.warn(`Le nombre de feuille (leaf) maximum (${maxCount}) a été atteint.`)
			return
		}
		
		// https://github.com/mrdoob/three.js/blob/master/examples/webgl_instancing_modified.html#L171
		dummy.position.set(x, y, z)
		dummy.scale.setScalar(scale)
		if (quaternion) {
			dummy.quaternion.copy(quaternion)
		}
		dummy.updateMatrix()

		dummyColor.set(randomItem([orange, '#fc0', 'white']))

		instancedMesh.setMatrixAt(index, dummy.matrix)
		instancedMesh.setColorAt(index, dummyColor)

		index += 1
	}

	return { instancedMesh, addLeaf }
}