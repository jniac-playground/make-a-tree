import * as THREE from '../three/three.module.js'
import { randomItem } from '../utils.js'
import { createMesh } from './basic/mesh.js'
import { textureLoader } from '../stage/loaders.js'

const leafMap = textureLoader.load('./assets/leaf-1.png')
const leafGeometry = new THREE.PlaneGeometry()
export const createLeaf = ({

	...props

} = {}) => {

	const leafMaterial = new THREE.MeshPhysicalMaterial({
		alphaMap: leafMap,
		alphaTest: 0.5,
		transparent: true,
		side: THREE.DoubleSide,
		color: randomItem(['#fc0', '#06c']),
	})

	const leaf = createMesh({
		...props,
		geometry: leafGeometry,
		material: leafMaterial,
		scale: .3 + .3 * Math.random(),
		rotationY: 360 * Math.random(),
		rotationZ: 360 * Math.random(),
		castShadow: true,
	})

	leaf.customDepthMaterial = new THREE.MeshDepthMaterial({
		depthPacking: THREE.RGBADepthPacking,
		alphaMap: leafMap,
		alphaTest: 0.5,
	})

	return leaf
}
