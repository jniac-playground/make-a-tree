import * as THREE from '../../three/three.module.js'
import { createMesh } from './mesh.js'

export const createBox = ({

  size = 1,
  width = size,
  height = size,
  depth = size,
  segments = 1,
  widthSegments = segments,
  heightSegments = segments,
  depthSegments = segments,
  ...meshProps
  
} = {}) => {

  const geometry = new THREE.BoxGeometry(width, height, depth, widthSegments, heightSegments, depthSegments)
  const cube = createMesh({
    geometry,
    ...meshProps,
  })

  return cube
}
