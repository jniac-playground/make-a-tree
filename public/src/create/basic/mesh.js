import * as THREE from '../../three/three.module.js'
import { DEBUG } from '../../config.js'
import { arrange } from './arrange.js'

const defaultGeometry = new THREE.BoxGeometry()

export const createMesh = ({

  color = 'white',
  wireframe = false,
  side = THREE.FrontSide,
  material = new THREE.MeshPhysicalMaterial({ color, wireframe, side }),

  geometry = defaultGeometry,

  castShadow = false,
  receiveShadow = false,

  onBeforeRender = null,
  onPointerDown = null,

  instance = false,
  instanceCount = 0,

  ...otherProps

} = {}) => {

  const mesh = (instance 
    ? new THREE.InstancedMesh(geometry, material, instanceCount) 
    : new THREE.Mesh(geometry, material)
  )
  arrange(mesh, otherProps)

  mesh.castShadow = castShadow
  mesh.receiveShadow = receiveShadow

  if (onBeforeRender !== null) {
    mesh.onBeforeRender = () => onBeforeRender(mesh)
  }

  if (onPointerDown !== null) {
    mesh.onPointerDown = onPointerDown
  }

  if (DEBUG === true) {
    
    // https://threejs.org/docs/#api/en/geometries/WireframeGeometry
    const wireframe = new THREE.WireframeGeometry(mesh.geometry)
    const line = new THREE.Line(wireframe)

    // color inversion
    const { r, g, b } = mesh.material.color
    line.material.color = new THREE.Color(1 - r, 1 - g, 1 - b)
    
    mesh.add(line)
  }

  return mesh
}
