import { scene } from '../../stage/stage.js'

const ensureNumber = (x, object) => {

	if (typeof x === 'function') {
		x = x(object)
	}

	if (typeof x === 'number') {
		return x
	}
	
	return 0
}

export const arrange = (object, {

	x = 0,
	y = 0,
	z = 0,
	rotationX = 0,
	rotationY = 0,
	rotationZ = 0,
	rotationDegree = true,
	rotationOrder = 'XYZ',  // https://threejs.org/docs/#api/en/math/Euler
	scale = 1,
	scaleX = undefined,
	scaleY = undefined,
	scaleZ = undefined,
	parent = scene,
	onFrame = null,
  
} = {}) => {

	x = ensureNumber(x, object)
	y = ensureNumber(y, object)
	z = ensureNumber(z, object)
	rotationX = ensureNumber(rotationX, object)
	rotationY = ensureNumber(rotationY, object)
	rotationZ = ensureNumber(rotationZ, object)
	scale = ensureNumber(scale, object)
	scaleX = ensureNumber(scaleX ?? scale, object)
	scaleY = ensureNumber(scaleY ?? scale, object)
	scaleZ = ensureNumber(scaleZ ?? scale, object)

	if (rotationDegree) {
		rotationX = rotationX * Math.PI / 180
		rotationY = rotationY * Math.PI / 180
		rotationZ = rotationZ * Math.PI / 180
	}

	parent?.add(object)

	object.position.set(x, y, z)
	object.rotation.set(rotationX, rotationY, rotationZ, rotationOrder)
	object.scale.set(scaleX, scaleY, scaleZ)

	object.onFrame = onFrame
}