import * as THREE from '../three/three.module.js'
import { createBox } from './basic/box.js'

export const createSky = ({ 
	color = '#99ccff',
	...otherProps
  }) => {
	
	const material = new THREE.MeshBasicMaterial({
	  color,
	  side: THREE.BackSide,
	})
  
	const sky = createBox({
		size: 100,
		material,
		...otherProps,
	})
  
	return sky
  }
  