import * as THREE from '../three/three.module.js'
import { DEBUG } from '../config.js'
import { scene } from '../stage/stage.js'

export const createLights = () => {

	const ambient = new THREE.AmbientLight('white', 0.7)
	scene.add(ambient)

	const directional = new THREE.DirectionalLight('white', 0.3)
	directional.position.set(5, 50, 15)
	scene.add(directional)

	directional.castShadow = true
	directional.shadow.camera.near = .1
	directional.shadow.camera.far = 2500
	directional.shadow.bias = 0.0001

	directional.shadow.mapSize.width = 1024
	directional.shadow.mapSize.height = 1024

	if (DEBUG === true) {
		const directionalHelper = new THREE.DirectionalLightHelper(directional)
		scene.add(directionalHelper)
	}

	return { ambient, directional }
}
