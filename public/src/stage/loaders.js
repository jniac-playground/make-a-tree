import * as THREE from '../three/three.module.js'
import { FBXLoader } from '../three/from-examples/FBXLoader.js'

// utilisé ailleurs pour pouvoir charger des textures (jpeg/png)
export const textureLoader = new THREE.TextureLoader()

export const fbxLoader = new FBXLoader()

