import * as THREE from './three/three.module.js'
import { createBranch } from './create/branch.js'
import { createGround } from './create/ground.js'
import { createInstancedLeaves } from './create/leaves.js'
import { createLights } from './create/lights.js'
import { createLineRing } from './create/lineRing.js'
import { createSky } from './create/sky.js'
import { camera, canvas, scene } from './stage/stage.js'
import { OrbitControls } from './three/from-examples/OrbitControls.js'
import { lerp, radians } from './utils.js'
import './UI.js'

scene.rotation.y = radians(45)

camera.position.set(0, 3, 10)

const controls = new OrbitControls(camera, canvas)
controls.target.set(0, 1.7, 0)
controls.update()



createLights()
createSky({ color:'#385EBE' })

// pour le fun, la déco, le style
for (let i = 0; i < 5; i++) {
	createLineRing({
		y: 1.5,
		rotationY: -45,
		radius: 4.75 + .1 * i,
	})
}

createGround({ 
	gridWidth: 5,
	gridHeight: 5,
})

createGround({ 
	gridWidth: 7,
	gridHeight: 7,
	gridSpacing: 1,
	blockProps: { 
		size:.75, 
		rotationY:() => lerp(-30, 30, Math.random()),
		scale: () => lerp(.5, 1, Math.random()),
	},
	skip: (x, y) => (x > 0 && x < 7 - 1 && y > 0 && y < 7 - 1) || Math.random() < .5,
})

createGround({ 
	gridWidth: 9,
	gridHeight: 9,
	gridSpacing: 1,
	blockProps: { 
		size:.5, 
		rotationY:() => lerp(-90, 90, Math.random()),
		scale: () => lerp(.5, 1, Math.random()),
	},
	skip: (x, y) => (x > 0 && x < 9 - 1 && y > 0 && y < 9 - 1) || Math.random() < .75,
})


// tricky: les feuilles !

const leaves = createInstancedLeaves()

const Q = new THREE.Quaternion()
const Q2 = new THREE.Quaternion()
const P = new THREE.Vector3()
const S = new THREE.Vector3()
const axis = new THREE.Vector3(0, 1, 0)
const createNewLeaf = (branch) => {

	const localPosition = new THREE.Vector3(0, 2, 0)
	const worldPosition = localPosition.applyMatrix4(branch.matrixWorld)

	// https://stackoverflow.com/questions/21557341/three-js-get-world-rotation-from-matrixworld
	branch.matrixWorld.decompose(P, Q, S)

	const max = 5
	for (let i = 0; i < max; i++) {

		let { x, y, z } = worldPosition
		const delta = .05
		x += lerp(-delta, delta, Math.random())
		y += lerp(-delta, delta, Math.random())
		z += lerp(-delta, delta, Math.random())
		
		const scale = lerp(.4, .8, Math.random())
		
		const angle = Math.random() * Math.PI * 2
		Q2.setFromAxisAngle(axis, angle)
		Q.multiply(Q2)

		leaves.addLeaf({ x, y, z , scale, quaternion:Q })
	}
}

createBranch({
	rotationY: -45,
	scale: .8,
	recursivity: 8, 
	onEnding: createNewLeaf,
})

createBranch({
	x: 2,
	z: 2,
	scale: .35,
	recursivity: 4, 
	onEnding: createNewLeaf,
})

createBranch({
	x: -2,
	z: -1,
	rotationY: 25,
	scale: .3,
	recursivity: 3, 
	onEnding: createNewLeaf,
})






// debug

const debugMain = async () => {
	const stage = await import('./stage/stage.js')
	Object.assign(window, { 
		camera,
		controls,
		stage,
		controls,
		THREE,
	})
}
debugMain()
