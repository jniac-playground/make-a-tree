# Make a tree
Vanilla but Three JS

[online demo](https://jniac-playground.gitlab.io/make-a-tree/)

<a href="https://jniac-playground.gitlab.io/make-a-tree/">
<img src="./readme/screenshot-2.jpg" width="800">
</a>

## Programmation javascript, enjeux
Les notions abordées concernent : 
- "Passing props" (composition de fonctions, [spread operator](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Spread_syntax))
- Géométrie dans l'espace ([Vector3](https://threejs.org/docs/#api/en/math/Vector3), [Euler](https://threejs.org/docs/#api/en/math/Euler), [Matrix4](https://threejs.org/docs/#api/en/math/Matrix4), [Quaternion](https://threejs.org/docs/#api/en/math/Quaternion))
- La récursivité
- Optimisation du rendu avec les instances GPU ([InstancedMesh](https://threejs.org/docs/#api/en/objects/InstancedMesh))
